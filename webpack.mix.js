const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/pages/criteria', 'public/js/pages')
    .js('resources/js/pages/alternatives', 'public/js/pages')
    .js('resources/js/pages/dashboard', 'public/js/pages')
    .js('resources/js/pages/rating', 'public/js/pages')
    .js('resources/qdev/js/handlers', 'public/js/utils')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();
