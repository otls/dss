# Basic Decision Support System
A [Laravel 8.x](https://github.com/laravel/laravel) based project starter for any decision  support system. It provides `alternaitves`, `criteria`, `criteria's value` and `ratings` management. It just needs additional process for the method and adjustment for the attributes of alternaitves and criteria.

## Requirements
- PHP 7.3 above
- Composer 2.2 (tested)
- MySql 5.6 above
- NodeJS 11 bove

## Installation
Make sure the `.env` has been configured
 ```sh
$ cd [project dir]
$ composer install
$ php artisan migrate --seed 
```
If wanna play with the assets (it uses [AdminLTE 3.5](https://github.com/ColorlibHQ/AdminLTE))
 ```sh
$ cd [project dir]
$ npm install
$ npm run dev
```
