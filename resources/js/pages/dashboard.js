var Highcharts = require('highcharts');
require('highcharts/modules/exporting')(Highcharts);

$(() => {
    Highcharts.chart(
        'chart-section',
        {
            title: {
                text: 'Last Selection'
            },

            subtitle: {
                text: _chart_.date
            },

            xAxis: {
                categories: _chart_.categories
            },

            series: [{
                type: 'column',
                colorByPoint: true,
                data: _chart_.values,
                showInLegend: false,
                name: 'nilai'
            }]
        }
    );

})
