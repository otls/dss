$(async () => {
    let uri = await geturis();
    $('.form-validation').validate();
    $('#rating-table').DataTable();
    $.ajax({
        type: "GET",
        url: uri['rating.form'].replace('{alternative}', _ALTERNATIVE_.id),
        data: {},
        dataType: "json",
        success: function (response) {
            $('#detail_rating_modal_body').html(response)
        },
        error: (xhr, status, error) => {
            notify(error, 'error')
        }
    });

    $(document).on('submit', '#rating-detail-form', (e) => {
        e.preventDefault();
        $.ajax({
            type: "PUT",
            url: uri['rating.update'].replace('{rating}', _ALTERNATIVE_.rating.id),
            data: $(e.currentTarget).serialize(),
            success: function (response) {
                $('#detail_rating_modal').modal('hide');
                notify('Success');
                response.rating_values.map((val, i) => {
                    const label = `${val.label} (${val.value})`;
                    $(`#val-${val.uniqid}`).html(label);
                })
            },
            error: (xhr, status, error) => {
                notify(error, 'error');
            },
            beforeSend: () => {
                $('#detail_rating_modal_loading').removeClass('d-none');
            },
            complete: () => {
                $('#detail_rating_modal_loading').addClass('d-none');
            }
        });
    });
});
