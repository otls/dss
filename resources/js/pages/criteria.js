$(async () => {
    let datatable = {
        ajax: {
            type: 'GET',
            url: window.location.href
        },
        columns: [
            {
                name: 'crt_name',
                data: 'crt_name',
                className: 'align-middle text-capitalize'
            },
            {
                name: 'crt_type',
                data: 'crt_type',
                className: 'align-middle text-capitalize'
            },
            {
                name: 'crt_weight',
                data: 'crt_weight',
                className: 'align-middle text-capitalize'
            },
            {
                name: 'crt_value_tag',
                data: 'crt_value_tag',
                className: 'align-middle'
            },
            {
                name: 'crt_description',
                data: 'crt_description',
                className: 'align-middle'
            },
        ]
    };

    renderDatatable(datatable, '#criteria-table');
})
