let datatable = null;
let uri = null;

$(async () => {
    uri = await geturis();
    datatable = {
        ajax: {
            type: 'GET',
            url: window.location.href
        },
        columns: [
            {
                name: 'alt_name',
                data: 'alt_name',
                className: 'align-middle',
                render: (data, type, row, meta) => {
                    let ratingUrl = uri['rating.show'].replace("{alternative}", row.id);
                    return `
                      <a class="text-navy" href="${ratingUrl}"><i class="fas fa-link text-xs"></i> ${data}</a>
                    `
                }
            },
            {
                name: 'alt_description',
                data: 'alt_description',
                className: 'align-middle'
            },
            {
                name: 'id',
                data: 'id',
                orderable: false,
                searchable: false,
                className: 'align-middle text-center',
                render: (value, type, row, meta) => {
                    let ratingUrl = uri['rating.show'].replace("{alternative}", row.id);
                    let detail = `
                        <a
                            title="Show detail"
                            class=" btn-show-detail"
                            data-id="${value}" id="btnShowDetail${value}"
                            href="${ratingUrl}">
                            <i class="fas fa-info-circle text-muted"></i>
                        </a>`;
                    let actions = datatableRenderActions(value);
                    return detail + actions;
                }
            },
        ],
        order: [0, 'asc']
    };

    datatable = renderDatatable(datatable, '#alternatives-table');
});

$(document).on('click', '#alternative_button_add', () => {
    $('#alternatives_form').attr('data-state', 'post');
    $('#alternatives_form').attr('action', window.location.href);
    $('#alternatives_form_modal').modal('show');
});

$(document).on('click', '.btn-update', async (e) => {
    let dataId = $(e.currentTarget).attr('data-id');
    let url = $(e.currentTarget).attr('data-href') ?? `${window.location.href}/${dataId}`;

    $('#alternatives_form').attr('data-state', 'put');
    $('#alternatives_form').attr('action', url);
    await editData(url, 'alternatives');
});

$(document).on('submit', '#alternatives_form', async (e) => {
    e.preventDefault();
    await updateOrCreateData(e, 'alternatives');
    datatable.ajax.reload();
});

$(document).on('hidden.bs.modal', '#alternatives_form_modal', (e) => {
    $('#alternatives_form > .form-group > .form-control').removeClass('is-invalid');
    $('#alternatives_form').trigger('reset');
});


$(document).on('click', '.btn-remove', async (e) => {
    await destroyData(e);
    datatable.ajax.reload();
})
