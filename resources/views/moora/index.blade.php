
       <div id="decision-card" class="card border-none shadow-sm elevation-1">
           <div class="card-body">
               <div class="w-100 bg-white border-0">
                   <h3 class="card-text h5 mb-4">Decision Table</h3>
                   <table class="table table-result table-bordered table-hover table-md" id="decision-table">
                       {!! $header !!}
                       @foreach ($ratings as $items)
                           <tr>
                               <td>{{ $items['alternative'] }}</td>
                               @foreach ($items['ratings'] as $item)
                                   <td class="text-center">{{ $item }}</td>
                               @endforeach
                           </tr>
                       @endforeach
                   </table>
               </div>
           </div>
       </div>
       <div class="card border-none shadow-sm elevation-1">
           <div class="card-body">
               <div class="w-100 bg-white border-0">
                   <h3 class="card-text h5 mb-4">Normalization Table</h3>
                   <table class="table table-result table-bordered table-hover table-md" id="decision-table">
                       {!! $header !!}
                       @foreach ($selection['result'] as $k => $items)
                           <tr>
                               <td>{{ $items['alternative'] }}</td>
                               @foreach ($items['method']['normalized'] as $item)
                                   <td class="text-center">{{ $item }}</td>
                               @endforeach
                           </tr>
                       @endforeach
                   </table>
               </div>
           </div>
       </div>
       <div class="card border-none shadow-sm elevation-1">
           <div class="card-body">
               <div class="w-100 bg-white border-0">
                   <h3 class="card-text h5 mb-4">Optimization Table</h3>
                   <table class="table table-result table-bordered table-hover table-md" id="decision-table">
                       <thead>
                           <tr>
                               <th>Alternative</th>
                               <th>Optimized Value</th>
                           </tr>
                       </thead>
                       @foreach ($selection['result'] as $k => $items)
                           <tr>
                               <td>{{ $items['alternative'] }}</td>
                               <td class="">{{ $items['method']['optimized'] }}</td>
                           </tr>
                       @endforeach
                   </table>
               </div>
           </div>
       </div>
       <div class="card border-none shadow-sm elevation-1">
           <div class="card-body">
               <div class="w-100 bg-white border-0">
                   <h3 class="card-text h5 mb-4">Ranking Table</h3>
                   <table class="table table-result table-ranking table-bordered table-hover table-md" id="decision-table">
                       <thead>
                           <tr>
                               <th class="text-center">Rank</th>
                               <th>Alternative</th>
                               <th>Value</th>
                           </tr>
                       </thead>
                       @foreach ($selection['ranking'] as $k => $items)
                           <tr>
                               <td class="text-center">#{{ $loop->iteration	 }}</td>
                               <td>{{ $items['alternative'] }}</td>
                               <td class="">{{ $items['value'] }}</td>
                           </tr>
                       @endforeach
                   </table>
               </div>
           </div>
           <div class="card-footer">
                <a id="save-result-btn" onclick="return false" class="btn btn-default bg-navy rounded">Save Result</a>
           </div>
       </div>

    <script>
        window._results_ = @json($selection['ranking']);
        $(() => {
            let config = {
                'ordering': false,
                'responsive': true
            }
            $('.table-result').not('.table-ranking').DataTable(config);
            $('.table-ranking').DataTable({
                ...config,
                'ordering': false,
                'sort': [1, "desc"]
            });
        });

        $(document).on('click', '#save-result-btn', async (e) => {
            let results = {
                _token: $('meta[name=csrf-token]').attr('content')
            };
            results['results'] = _results_;

            $.ajax({
                type: "POST",
                url: window._uris['rating.saveHistory'],
                data: results,
                dataType: "json",
                success: function (response) {
                    notify("Saved successfuly");
                },
                error: (xhr, status, error) => {
                    notify(xhr.responseJSON.message, 'error')
                }
            });
        });
    </script>
