@extends('layouts.app')

@section('top-title')
    Alternatives
@endsection

@section('content')
    <div class="container">
        <div class="card border-none shadow-sm elevation-1">
            <div class="card-body">
                <h3 class="card-text h5 mb-4">Alternatives Management</h3>
                <table id="alternatives-table" class="table table-bordered table-hover table-md w-100">
                    <thead>
                        <th>Name</th>
                        <th>Description</th>
                        <th width="10%">#</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('utilities.floating_button', ['id' => "alternative_button_add"])
@endsection

@push('bottom')
    @include('utilities.center_modal', [
        'id' => 'alternatives_form_modal',
        'content' => 'alternative.form',
        'title' => 'Alternative Form',
])
    <script src="{{ mix('js/pages/alternatives.js') }}"></script>
    <script>
        $(() => {
            $(document).on('click', '.btn-update', (e) => {
                $(`alternatives_form_modal`).modal('show')
            })
        })
    </script>
@endpush

@push('top')
    <script src="{{ mix('js/utils/handlers.js') }}"></script>
@endpush
