<form data-state="post" class="form-validation" id="alternatives_form" action="{{ route('alternatives.store') }}" method="post">
    @csrf
    <div class="form-group">
        <label for="alt_name">Name</label>
        <input type="text" name="alt_name" id="alt_name" class="form-control" placeholder="Insert alternative's name" />
        <span id="alt_name_error" class="invalid-feedback"></span>
    </div>
    <div class="form-group">
        <label for="alt_description">Description</label>
        <textarea name="alt_description" id="alt_description" class="form-control" placeholder="Insert alternative's description" cols="10" rows="5"></textarea>
        <span id="alt_description_error" class="invalid-feedback"></span>
    </div>
    <div class="form-row justify-content-between">
        <div class="col-md-6 form-group align-middle">
            <input type="checkbox" name="stay" id="stay" value="false">
            <label for="stay">Stay at this page</label>
        </div>
        <div class="form-group col-md-6 text-right">
            <button type="submit" class="btn btn-default bg-navy rounded">Save</button>
        </div>
    </div>

</form>
