@extends('layouts.app')

@section('content')
    <div class="container py-3">
    <div class="row">
        <div class="col-md-4">
            <div class="info-box elevation-1 shadow-sm">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Alternatives</span>
                <span class="info-box-number">
                  {{ $alternative }}
                  Data
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box elevation-1 shadow-sm">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Criterias</span>
                <span class="info-box-number">{{ $criteria }} Data</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <div class="col-md-4">
            <div class="info-box elevation-1 shadow-sm">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-history"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Selections</span>
                <span class="info-box-number">{{ $history }} Data</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
    </div>
        <div style="min-height: 50vh" class="card border-none shadow-sm">
            <div class="card-body elevation-1 shadow-sm">
                <div id="chart-section"></div>
            </div>
        </div>
    </div>
@endsection

@push('bottom')
    <script>
        window._chart_ = @json($chart, JSON_NUMERIC_CHECK);
    </script>
    <script src="{{ mix('js/pages/dashboard.js') }}"></script>
@endpush
