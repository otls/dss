<div class="modal border-0 elevation-3 shadow-lg" id="modal-filter" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-teal shadow-sm">
                <h5 class="mb-0 card-title text-uppercase font-weight-bold">Filter Data</h5>
                <a type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="text-white text-md" aria-hidden="true"><i class="fas fa-times"></i></span>
                </a>
            </div>
            <form class="form-validation" id="modal-filter-form" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-light">Start Date</label>
                            <input  type="date" name="f_start_date" id="f_start_date" class="form-control form-control-sm">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-light">End Date</label>
                            <input disabled  type="date" name="f_end_date" id="f_end_date" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label class="font-weight-light">Province</label>
                            <select name="f_province" id="f_province" class="form-control select2 f_region" style="width: 100%">
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label class="font-weight-light">Region</label>
                            <select name="f_regency" id="f_regency" class="form-control select2 f_region" style="width: 100%">

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="modal-filter-form-button" class="btn elevation-1 btn-sm bg-teal">Filter</button>
                    <button type="reset" class="btn elevation-1 btn-sm btn-secondary" id="btn-reset-filter">Reset</button>
                    {{-- <button type="reset" class="btn elevation-1 btn-sm btn-default" data-dismiss="modal" data-target="#modal-filter">Close</button> --}}
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript"  src="{{ asset('js/utilities/modal_filter.js') }}"></script>
