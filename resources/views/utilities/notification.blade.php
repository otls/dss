@if (isset($message))
    <script>
        $(() => {
            notify("{{$message}}", "{{isset($type) ? $type : "info"}}", "{{isset($title) ? $title : null}}");
        });
    </script>
@endif

@if (session('message'))
    <script>
        $(() => {
            notify("{{session('message')}}", "{{session('type') !== null ? session('type') : "info"}}", "{{session('title') != null ? session('title') : null}}");
        });
    </script>
@endif

