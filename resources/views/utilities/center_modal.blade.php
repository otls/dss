<!-- Modal -->
<div class="modal fade" id="{{ $id ?? 'center_modal' }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered border-0" role="document">
        <div class="modal-content">
            <div id="{{ $id ?? 'center_modal' }}_loading"
                class="overlay d-none justify-content-center align-items-center">
                <p class="mr-2"><i class="fas fa-2x fa-sync fa-spin text-white"></i>
                <p class="text-white">Please wait..</p>
            </div>
            <div class="modal-header bg-navy" id="{{ $id ?? 'center_modal' }}_header">
                @isset($title)
                    <h6 class="modal-title" id="modal_title">{{ $title }}</h6>
                @endisset
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-sm" id="{{ $id ?? 'center_modal' }}_body">
                @isset($content)
                    @include($content, $data ?? [])
                @endisset
            </div>
        </div>
    </div>
</div>
