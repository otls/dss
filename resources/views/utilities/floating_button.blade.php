<div style="position: fixed; z-index: 10; bottom: 10%; right: 10%;">
        <a title="Add new data" id="{{ $id ?? "floating_button" }}" class="btn shadow btn-default btn-sm bg-navy text-center rounded-circle"><i class="fas fa-plus mx-1 my-2"></i></a>
</div>
