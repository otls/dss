<div id="{{ $spinner_id ?? 'loading-spinner' }}" class="overlay d-none {{ $spinner_class ?? '' }}" >
      <i class="fas fa-2x fa-spinner fa-pulse "></i>
</div>
