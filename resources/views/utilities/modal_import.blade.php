<div class="modal border-0 elevation-3 shadow-lg" id="modal-import" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-teal shadow-sm">
                <h5 class="mb-0 card-title text-uppercase font-weight-bold">Import File</h5>
                <a type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="text-white text-md" aria-hidden="true"><i class="fas fa-times"></i></span>
                </a>
            </div>
            <form class="form-validation" action="{{ route('data-master.store') }}" id="modal-import-form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        {{-- <label class="font-weight-light">File xlsx</label> --}}
                        <input required  type="file" name="import_file" id="import_file" class="form-control form-control-sm">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="modal-import-form-button" class="btn elevation-1 btn-sm bg-teal">Import</button>
                    <button type="button" class="btn elevation-1 btn-sm btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
