
<div class="modal border-0 elevation-3 shadow-lg" id="modal-upload-picture" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            @include('utilities.loading_spinner', ['spinner_id' => "modal-spinner", "spinner_class" => "bg-white justify-content-center align-items-center"])
            <div class="modal-header bg-teal shadow-sm">
                <h5 class="mb-0 card-title text-uppercase font-weight-bold">Upload picture</h5>
                <a type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="text-white text-md" aria-hidden="true"><i class="fas fa-times"></i></span>
                </a>
            </div>
            <form data-store={{ route('data-pictures.store') }} class="form-validation" action="{{ route('data-pictures.store') }}" id="modal-upload-picture-form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <input required  type="file" name="pict_file" id="pict_file" class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="pict_type">Picture Type</label>
                            <select data-placeholder="Choose Picture Type" required name="pict_type" style="width: 100%"  id="pict_type" class="form-control select2 @error('pict_type') is-invalid @enderror">
                                @foreach(config('tbkemenkes.picture_type') as $item => $value)
                                    <option @if( (old('pict_type') == $value) || (isset($data->id) and ($data->pict_type == $value) ) ) selected @endif value="{{ $value }}">{{ ucwords(str_replace('_', " ", $value)) }}</option>
                                @endforeach
                            </select>
                            @error('pict_type')
                                <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pict_desc">Description</label>
                        <input type="text" name="pict_desc" id="pict_desc" class="form-control" placeholder="Input picture description"/>
                    </div>
                    <div class="form-group d-none" id="pict-review-box">
                        <div style="max-width: 200px; height: 120px;" class="bg-grey d-flex align-items-middle justify-content-center border rounded">
                            <img id="pict-review-img" class="mx-auto" style="max-width: inherit; height: 100%;" src="{{ asset('images/celebrating.svg') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row justify-content-between align-middle">
                        <div class="col-md-8">
                            <p class="text-xs">Untuk hasil yang lebih maksimal, usahakan mengunggah foto dengan orientasi potret</p>
                        </div>
                        <div class="col-md-4">
                            <div class="d-flex flex-row">
                                <button type="submit" id="modal-upload-picture-form-button" class="btn elevation-1 btn-sm bg-teal mx-1">Upload</button>
                                <button type="button" class="btn elevation-1 btn-sm btn-secondary mx-1" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(() => {
        $('.select2').select2({
            'placeholder': "Choose picture type"
        });

        $('#modal-upload-picture').on('hide.bs.modal', (e) => {
            let act = $('#modal-upload-picture-form').attr('data-store');
            $('#pict-review-box').addClass('d-none');
            $('#modal-upload-picture-form').attr('action', act).trigger('reset');
            $('#modal-upload-picture-form > input[name=_method]').remove();
            $('#modal-spinner').removeClass(['d-block', 'd-flex', 'd-inline', 'd-inline-block']).addClass('d-none');
            setValidation();
            window.fValidation.resetForm();
        });

        setValidation();

        document.getElementById("pict_file").onchange = function (e) {
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("pict-review-img").src = e.target.result;
            };
            reader.readAsDataURL(this.files[0]);
            $('#pict-review-box').removeClass('d-none');
        };

    });

function setValidation() {
    window.fValidation = $('#modal-upload-picture-form').validate({
            rules: {
                pict_file: {
                    required: true,
                    extension: "png|jpg|jpeg|gif"
                },
                pict_type: {
                    required: true
                }
            }
        });
}
</script>
