<ul class="navbar-nav">
    <li class="nav-item {{ request()->is('admin') ? 'active' : '' }}">
        <a href="{{ route('home') }}" class="nav-link">Home</a>
    </li>
    <li class="nav-item {{ request()->is('admin/criteria*') ? 'active' : '' }}">
        <a href="{{ route('criteria.index') }}" class="nav-link">Criteria</a>
    </li>
    <li class="nav-item {{ request()->is('admin/alternatives*') ? 'active' : '' }}">
        <a href="{{ route('alternatives.index') }}" class="nav-link">Alternatives</a>
    </li>
    <li class="nav-item {{ request()->is('admin/selection*') ? 'active' : '' }}">
        <a href="{{ route('rating.index') }}" class="nav-link">Selection</a>
    </li>

    <li class="nav-item d-inline d-md-none ">
        <form action="{{ route('logout') }}" method="post">
            @csrf
            <button type="submit" class="dropdown-item nav-link border-0 btn btn-link">
                Logout
            </button>
        </form>
    </li>
</ul>
