@extends('layouts.app')

@section('top-title')
    Criteria
@endsection

@section('content-header')
    {{-- <div class="container">
        <h3 class="h4">Criteria Management</h3>
    </div> --}}
@endsection

@section('content')
    <div class="container">
        <div class="card border-none shadow-sm elevation-1">
            <div class="card-body">
                <h3 class="card-text h5 mb-4">Criteria Management</h3>
                <table id="criteria-table" class="table table-bordered table-hover table-md">
                    <thead>
                        <th>Criteria</th>
                        <th>Type</th>
                        <th>Weight</th>
                        <th>Value Tag</th>
                        <th>Description</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('bottom')
    <script src="{{ mix('js/pages/criteria.js') }}"></script>
@endpush
