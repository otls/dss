<thead>
    <tr>
        <th>Alternative</th>
        @foreach ($criteria as $item)
            <th class="text-center">{{ $item }}</th>
        @endforeach
    </tr>
</thead>
