@extends('layouts.app')

@section('top-title')
    Selection
@endsection

@section('content')
    <div class="container" id="content-wrapper">
        <div class="card border-none shadow-sm elevation-1">
            <div class="card-body">
                <div class="w-100 bg-white border-0 d-flex flex-row mb-3">
                    <h3 class="card-text h5 mb-0">Rating Table</h3>
                    <a href="" onclick="return false" id="start-counting-btn" class="btn btn-sm ml-auto btn-default shadow-sm elevation-1"><i class="fas fa-play text-green"></i> Start Counting</a>
                </div>
                <table id="rating-table" class="table table-bordered table-hover table-md">
                    <thead>
                        @foreach ($tableHeader as $k => $header)
                            <th>{{ $header }}</th>
                        @endforeach
                    </thead>
                    <tbody id="rating-table-body">
                        <tr>
                            <td class="text-center" colspan="6">
                                  <i class="fas fa-sync fa-spin"></i>
                                  <p class="mb-0">loading</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="loading" class="my-5 text-center d-none">
            <i class="fas fa-sync fa-spin"></i>
            <p class="mb-0">loading</p>
        </div>
        <div id="result"></div>
    </div>
@endsection

@push('bottom')
    <script>
        $(() => {
            $.ajax({
                type: "GET",
                url: '',
                data: {},
                dataType: "json",
                success: function (response) {
                    $('#rating-table').DataTable({
                        order: [0, 'asc'],
                        data: response,
                        responsive: true,
                    });
                },
                error: (xhr, status, error) => {
                    notify(error, 'error');
                }
            });
        });
        $(document).on('click', '#start-counting-btn', (e) => {
            $.ajax({
                type: "GET",
                url: "selection/start",
                data: {},
                dataType: "json",
                success: function (response) {
                    $('#result').html(response);
                    let resultSection =  $("#decision-card").position().top;

                    $("HTML, BODY").animate({
                        scrollTop: resultSection - 190
                    }, 1000);
                },
                error: (xhr, status, error) => {
                    notify(error, 'error');
                },
                beforeSend: () => {
                    $('#result').empty();
                    $('#loading').removeClass('d-none');
                },
                complete: () => {
                    $('#loading').addClass('d-none');
                }
            });
        });
    </script>
@endpush
