@extends('layouts.app')

@section('top-title')
    {{ $alternative->alt_name }}
@endsection

@section('content')
    <div class="container">
        <div class="card border-none shadow-sm elevation-1">
            <div class="card-header w-100 bg-white border-0 d-flex flex-column flex-md-row">
                <h3 class="card-text h5 mb-0">{{ $alternative->alt_name }}'s Rating Detail</h3>
                <a href="" data-toggle="modal" data-target="#detail_rating_modal" onclick="return false" class="btn btn-sm ml-1 mt-3 mt-md-0 ml-md-auto btn-default shadow-sm elevation-1"><i class="fas fa-edit"></i> Update</a>
            </div>
            <div class="card-body">
                <table id="rating-table" class="table table-bordered table-hover table-md">
                    <thead>
                       <th>Criteria</th>
                       <th>Value</th>
                    </thead>
                    <tbody>
                        @foreach ($alternative->rating_values as $item)
                            <tr>
                                <td class="align-middle text-capitalize" id="crt-{{ $item->uniqid }}">{{ $item->criteria }}</td>
                                <td class="align-middle" id="val-{{ $item->uniqid }}">
                                    {{ $item->label }} ({{ $item->value }})
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('utilities.center_modal', [
        'id' => "detail_rating_modal",
        'title' => 'Update Rating Value'
    ])
    <script>
        window._ALTERNATIVE_ = @json($alternative)
    </script>
@endsection

@push('bottom')
    <script src="{{ mix('js/pages/rating.js') }}"></script>
@endpush
