<form class="form-validation" id="rating-detail-form" action="" method="post">
    @csrf
    @foreach ($criteria as $item)
        <div class="form-group">
            <label for="crt_label_{{ $item->id }}">{{ $item->crt_name }}</label>
            <select required class="form-control w-100 select2" name="c_{{ $item->id }}" id="crt_select_{{ $item->id }}">
                <option value=""></option>
                @foreach ($item->values as $value)
                    <option
                        id="crtopt{{ $item->id }}{{ $value->id }}{{ $value->critval_tag }}"
                        data-label="{{ $value->critval_label }}"
                        value="{{ $value->id }}">
                        {{ $value->critval_label }} ({{ $value->critval_value }})
                    </option>
                @endforeach
            </select>
        </div>
    @endforeach
    <div class="form-group text-right">
        <button type="submit" class="btn btn-default bg-navy rounded">Save</button>
    </div>
</form>

<script>
    $(() => {
        let ratings = @json($ratings);
        $('.form-validation').validate();
        $('.select2').select2({
            placeholder: "Select a value",
        })
        ratings.map((val, i) => {
            const id = `#crtopt${val.criteria_id}${val.value_id}${val.value_tag}`
            $(id).prop('selected', true);
            $(id).trigger('change')
        })
    })
</script>
