window.datatableRenderActions = (data) => {
    let updateBtn = `<a title="Edit data" class="mx-1 btn-update" data-id="${data}" id="btnUpdate${data}" href="" onclick="return false"><i class="fas fa-pencil-alt text-muted"></i></a>`;
    let removeBtn = `<a title="Delete data" class="mx-1 btn-remove" data-id="${data}" id="btnRemove${data}" href="" onclick="return false"><i class="fas fa-times text-danger text-muted"></i></a>`;
    return updateBtn + removeBtn;
}

window.formValidationErrorHandler = (xhr, status, error) => {
    const errors = xhr.responseJSON;
    for (const k in errors.errors) {
        if (Object.hasOwnProperty.call(errors.errors, k)) {
            const element = errors.errors[k].join(', ');
            $(`#${k}_error`).text(element);
            $(`#${k}`).addClass('is-invalid');
        }
    }
}

window.editData = async (url, object) => {

    return await $.ajax({
        type: "GET",
        url: url,
        data: {},
        dataType: "json",
        success: function (response) {
            formPopulator(response);
            $(`#${object}_form_modal`).modal('show');
        }
    });
}

window.updateData = async (e, object) => {
    return await updateOrCreateData(e, object);
}

window.createData = async (e, object) => {
    console.log(e);
    return await updateOrCreateData(e, object);
}

const destroyConfirmation = async () => {
    return await Swal.fire({
        title: `Are you sure want to delete this data ?`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#001f3f',
        cancelButtonColor: '#d33',
        confirmButtonText: `Ya, sure!`
    }).then((result) => {
        return result.value
    });
}

window.destroyData = async (e) => {
    let dataId = $(e.currentTarget).attr('data-id');
    let url = $(e.currentTarget).attr('data-href') ?? `${window.location.href}/${dataId}`;
    let token = $('meta[name=csrf-token]').attr('content');
    let datatable = $(e.currentTarget).closest("table");

    const confirm = await destroyConfirmation();
    if (!confirm) return;

    return await $.ajax({
        type: "delete",
        url: url,
        data: { _token: token },
        dataType: "json",
        success: function (response) {
            notify('Data successfuly deleted');
            return response;
        },
        error: (xhr, status, error) => {
            notify(error, 'error')
        }
    });
}

window.formPopulator = (data) => {
    for (const col in data) {
        if (Object.hasOwnProperty.call(data, col)) {
            const value = data[col];
            $(`#${col}`).val(value);
        }
    }
}

window.updateOrCreateData = async (e, object) => {

    const data = $(e.currentTarget).serialize();
    const url = $(e.currentTarget).attr('action');
    const method = $(e.currentTarget).attr('data-state');

    let form = `#${object}_form`;
    let modal = `#${object}_form_modal`;
    let modalLoading = `#${object}_form_modal_loading`;

    return await $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: "json",
        success: function (response) {
            if (typeof object == 'function') {
                return object(response);
            }
            notify('Done successfuly');
            $(form).trigger('reset');
            $(modal).modal('hide');
            return response;
        },
        error: (xhr, status, error) => {
            if (xhr.status == 422) {
                return formValidationErrorHandler(xhr, status, error);
            }
            notify(error, 'error');
        },
        complete: () => {
            $(modalLoading).removeClass('d-flex').addClass('d-none');
        },
        beforeSend: () => {
            $(modalLoading).removeClass('d-none').addClass('d-flex');
        }
    });
}
