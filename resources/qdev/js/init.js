window._ = require('lodash');
let path = require('path');
/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */
try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {
 }

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

//jquery ui
require('admin-lte/plugins/jquery-ui/jquery-ui');

// datatables
require('admin-lte/plugins/datatables/jquery.dataTables');
require('admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4');
require('admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4');
require('admin-lte/plugins/datatables-buttons/js/dataTables.buttons');
require('admin-lte/plugins/datatables-buttons/js/buttons.bootstrap4');

// toastr
window.toastr = require('admin-lte/plugins/toastr/toastr.min');
//daterangepicker
// require('admin-lte/plugins/daterangepicker/daterangepicker');
//validation
require('admin-lte/plugins/jquery-validation/jquery.validate');
require('admin-lte/plugins/jquery-validation/additional-methods');
require('admin-lte/plugins/jquery-validation/localization/messages_id');
jQuery.validator.setDefaults({
    errorElement: 'div',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
// scroll bar
require('admin-lte/plugins/overlayScrollbars/js/OverlayScrollbars');
require('admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars');
//select2
require('admin-lte/plugins/select2/js/select2.full');
$.fn.select2.defaults.set("theme", "bootstrap4");
//sweetalert2
window.Swal = require('admin-lte/plugins/sweetalert2/sweetalert2.all');
// pacejs
require('admin-lte/plugins/pace-progress/pace');
window.paceOptions = {
    // Disable the 'elements' source
    elements: true,
    ajax: true,
}
require('moment');
Window.moment = require('moment');
window.Moment = require('moment');
//jquery knob
require('admin-lte/plugins/jquery-knob/jquery.knob.min');
//bootstrap switch
require('admin-lte/plugins/bootstrap-switch/js/bootstrap-switch');
// jquery form
require('../../vendor/jquery-form/jquery-form');
//bs custom file input
window.bsCustomFileInput = require('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input');
// adminlte 3
require('admin-lte');




