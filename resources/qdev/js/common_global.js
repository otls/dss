$(async () => {

    window._uris = await geturis();
    if ($(window).width() < 768) {
        $('[data-toggle="tooltip"]').tooltip('disable');
    } else {
        $('[data-toggle="tooltip"]').tooltip();
    }

    $("input[data-bootstrap-switch]").each(function () {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

});

$(document).on('load', 'form.form-validation', (e) => {
    $(e.currentTarget).validate();
})

window.geturis = async () => {

    // let base_url = null;
    let _token = $('meta[name=csrf-token]').attr('content');
    let base_url = $('meta[name=base-url]').attr('content');
    base_url += '/uriforbetterlife'

    let uris = await $.ajax({
        type: "POST",
        url: base_url,
        data: {
            _token: _token
        },
        dataType: "json",
        success: function (response) {
            return response
        },
        error: (xhr, status, error) => { },
        beforeSend: () => {
            $('#loading-spinner').removeClass('d-none');
        },
        complete: () => {
            $('#loading-spinner').addClass('d-none');
        }
    });
    return uris;
}

$(document).ajaxError((e, xhr) => {
    if(xhr.statusCode == 401) {
        window.location.href = '/';
    }
} )
