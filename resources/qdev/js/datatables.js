window.renderDatatable = (config = {}, selector = '.table') => {
    let defaultConfig = {
        "searching": true,
        "ordering": true,
        "paging": true,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        'autoWidth': true,
        "stateSave": true,
        "stateDuration": 60 * 60 * 4,
        "pageLength": 10,
        "pagingType": $(window).width() < 768 ? 'numbers' : 'simple_numbers',
        "language": {
            processing: `
                <div id="loading" class="text-center">
                    <i class="fas fa-sync fa-spin"></i>
                    <p class="mb-0">loading</p>
                </div>
            `
        }
    };

    let drawCallback = config.hasOwnProperty('drawCallback') ? config.drawCallback() : '';
    config = {
        ...defaultConfig,
        ...config
    }

    config.drawCallback = (setting) => {
        if (typeof setting == 'function') {
            drawCallback(setting);
        }
        $('#loading-spinner').addClass('d-none');
        $("html, body").animate(
            { scrollTop: "0" }, 1000);
    }

    config.ajax.beforeSend = () => {
        $('#loading-spinner').removeClass('d-none');
    }


    let datatable = $(selector).DataTable(config);
    return datatable;
}
