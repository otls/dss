<?php

/**
 * Name: MOORA DSS Method
 * Author: otls
 * Email: opicktl.official@gmail.com
 * Version: 1.0.0
 */

namespace App\Modules;

use Illuminate\Support\Facades\Log;

class Moora
{
    private $decisionMatrix = [];
    private $normalized = [];
    private $criteria = [];
    private $dividers = [];
    private $optimized = [];
    private $alternatives = [];
    private $result = [];
    private $ranking = [];

    public function __construct(array $ratings, array $criteria)
    {
        $this->ratings = $ratings;
        $this->criteria = $criteria;

        $this->setDecisionMatrix($ratings);
        $this->normalize($this->decisionMatrix, $this->dividers);
        $this->optimize($this->normalized);
        $this->setResult($this->optimized, $this->alternatives);
    }

    /**
     * set decision matrix for moora process
     * @param array $ratings ['alternative' => '', 'ratings' => [20, 90]]
     * @return void
     */
    private function setDecisionMatrix(array $ratings): void
    {
        foreach ($ratings as $k => $alternative) {
            $alt_id = $k + 1;
            $alt_id = "a{$alt_id}";
            $this->alternatives[$alt_id] = $alternative['alternative'];

            foreach ($alternative['ratings'] as $j => $value) {
                $i = $j + 1;
                $crt_id = "c{$i}";
                $this->decisionMatrix[$alt_id][$crt_id] = $value;
                $this->setDivider($crt_id, $value);
            }
        }
    }

    /**
     * set divider which is used for dividing rating value (no sqrt yet)
     * @param string $id created by setDecisionMatrix
     * @param float $value value of rating, created by setDecisionMatrix
     * @return void
     */
    private function setDivider(string $id, float $value): void
    {
        if (isset($this->dividers[$id])) {
            $this->dividers[$id] += pow($value, 2);
        } else {
            $this->dividers[$id] = pow($value, 2);
        }
    }

    /**
     * moora normalization process
     * @param array $decissionMatrix produced by setDecisionMatrix
     * @param array $dividers produced by setDivider
     * @return void
     */
    private function normalize(array $decissionMatrix, array $dividers): void
    {
        foreach ($decissionMatrix as $k => $ratings) {
            foreach ($ratings as $idx => $value) {
                $this->normalized[$k][$idx] = $value / sqrt($dividers[$idx]);
            }
        }
    }

    /**
     * moora optimization process
     * @param array $normalized produced by normalize
     * @return void
     */
    private function optimize(array $normalized): Void
    {
        foreach ($normalized as $j => $alt_rating) {
            $i = 0;
            $optimized_value = 0;

            foreach ($alt_rating as $k => $value) {
                $tmp_val = $value * $this->criteria[$i]['crt_weight'];
                if ($this->criteria[$i]['crt_type'] == 'benefit') {
                    $optimized_value += $tmp_val;
                } else {
                    $optimized_value -= $tmp_val;
                }
                $i++;
            }

            $this->optimized[$j] = $optimized_value;
        }
    }

    /**
     * set / reformat result
     * @param array $optimized produced by optimize
     * @param array $alternatives produced by setDecisionMatrix
     * @return void
     */
    private function setResult(array $optimized, array $alternatives): void
    {
        $ranking = [];
        foreach ($alternatives as $key => $name) {
            $ranking[] = [
                'value' => $optimized[$key],
                'alternative' => $name
            ];
            $this->result[] = [
                'alternative' => $name,
                'result' => $optimized[$key],
                'method' => [
                    'optimized' => $optimized[$key],
                    'normalized' => $this->normalized[$key],
                ]
            ];
        }
        $this->ranking($ranking);
    }

    private function getter($property = null)
    {
        if ($property) {
            return $this->$property;
        }
        return [
            'decisionMatrix' => $this->decisionMatrix,
            'normalized' => $this->normalized,
            'criteria' => $this->criteria,
            'dividers' => $this->dividers,
            'optimized' => $this->optimized,
            'alternatives' => $this->alternatives,
            'result' => $this->result,
            'ranking' => $this->ranking
        ];
    }

    private function ranking(array $optimized)
    {
        $ranking = collect($optimized)->sortByDesc('value');
        $this->ranking = $ranking;
    }

    public function getResult(): array
    {
        return $this->getter('result');
    }

    public function getOptimized(): array
    {
        return $this->getter('optimized');
    }

    public function getNormalized(): array
    {
        return $this->getter('normalize');
    }

    public function getDecisionMatrix(): array
    {
        return $this->getter('decisionMatrix');
    }

    public function getDivider(): array
    {
        return $this->getter('dividers');
    }

    public function getAll(): array
    {
        return $this->getter();
    }
}
