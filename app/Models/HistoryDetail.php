<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'history_id',
        'hd_alternative',
        'hd_value',
        'hd_rank'
    ];

    public function history()
    {
        return $this->belongsTo(History::class);
    }
}
