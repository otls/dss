<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;

    public function details()
    {
        return $this->hasMany(HistoryDetail::class);
    }

    public static function scopeLatestFormatChart($query)
    {
        $data = $query->latest()->first();

        $values = [];
        $categories = [];
        $date = null;

        if ($data) {
            $date = $data->created_at->format('d M Y');
            $details = $data->details;
            $details = $details->sortBy('hd_alternative');
            foreach ($details as $key => $detail) {
                $values[] = number_format($detail['hd_value'], '2');
                $categories[] = $detail['hd_alternative'];
            }
        }
        return [
            'values' => $values,
            'categories' => $categories,
            'date' => $date
        ];
    }
}
