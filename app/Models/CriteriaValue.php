<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CriteriaValue extends Model
{
    protected $fillable = [
        'critval_label',
        'critval_tag',
        'critval_value'
    ];
}
