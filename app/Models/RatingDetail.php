<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatingDetail extends Model
{
    protected $fillable = [
        'rating_id',
        'criteria_id',
        'criteria_value_id',
        'rating_detail_label',
        'rating_detail_value'
    ];

    public function criteria()
    {
        return $this->belongsTo(Criteria::class);
    }

    public function value()
    {
        return $this->belongsTo(CriteriaValue::class);
    }

    public function rating()
    {
        return $this->belongsTo(Rating::class);
    }
}
