<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alternative extends Model
{
    protected $fillable = [
        'alt_name',
        'alt_description'
    ];

    private $attributeLabels = [
        'alt_name' => 'Name',
        'alt_description' => 'Description'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        parent::booted();
        static::created(function ($alternative) {
            static::handleCreated($alternative);
        });
    }

    private static function handleCreated($alternative)
    {
        $alternative
            ->rating()
            ->create([
                'rating_periode' => date('Y-m-d H:i:s')
            ]);

        $criteria = Criteria::get();
        foreach ($criteria as $key => $item) {
            $alternative->rating->details()->create([
                'rating_detail_value' => 0,
                'rating_detail_label' => "No label",
                'criteria_id' => $item->id
            ]);
        }
    }

    public function getRatingvaluesAttribute()
    {
        $attributes = "
            FLOOR(rat_dt.rating_detail_value),
            rat_dt.id,
            CONCAT(rat_dt.id, crt.id) as uniqid,
            ratings.id as rating_id,
            crt.id as criteria_id,
            crt.crt_name as criteria,
            critval.id as value_id,
            critval.critval_tag as value_tag,
            rat_dt.rating_detail_value as value,
            rat_dt.rating_detail_label as label
       ";
        if ($this->rating) {
            $data = $this->rating
                ->join('rating_details as rat_dt', 'ratings.id', '=', 'rat_dt.rating_id')
                ->rightJoin('criterias as crt', 'crt.id', 'rat_dt.criteria_id')
                ->leftJoin('criteria_values as critval', 'rat_dt.criteria_value_id', 'critval.id')
                ->where('ratings.alternative_id', $this->id)
                ->orderBy('crt.id')
                ->selectRaw($attributes)
                ->get();
            return $data;
        }
        return ["no data"];
    }

    public static function getInputFieldsAttribute()
    {
        $alterntiveModel = new self;
        $fillable = $alterntiveModel->getFillable();
        $attributeLabels = $alterntiveModel->attributeLabels;

        $inputFields = collect($fillable)->transform(function ($item) use ($attributeLabels) {
            return [
                'label' => $attributeLabels[$item] ?? $item,
                'name'  => $item
            ];
        })->toArray();
        return $inputFields;
    }

    public function rating()
    {
        return $this->hasOne(Rating::class);
    }
}
