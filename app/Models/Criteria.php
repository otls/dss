<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    protected $fillable = [
        'crt_name',
        'crt_value_tag',
        'crt_weight',
        'crt_type',
        'crt_description'
    ];

    public static function getHeaderAttribute()
    {
        $headers = ['Alternative'];
        $criterias = self::select('crt_name')
            ->orderBy('id', 'asc')
            ->get()
            ->pluck('crt_name')
            ->toArray() ?? [];
        return array_merge($headers, $criterias);
    }

    public static function getHeaderWithHtml()
    {
        $criterias = self::select('crt_name')
        ->orderBy('id', 'asc')
        ->get()
            ->pluck('crt_name')
            ->toArray() ?? [];
        $header = view('criteria.header', ['criteria' => $criterias])->render();
        return $header;
    }

    public function getValuesAttribute()
    {
        $values = CriteriaValue::where('critval_tag', $this->crt_value_tag)->get();
        return $values;
    }
}
