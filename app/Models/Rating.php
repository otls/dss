<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = ['alternative_id', 'rating_periode'];
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:d-m-Y',
    ];

    public function alternative()
    {
        return $this->belongsTo(Alternative::class);
    }

    public function details()
    {
        return $this->hasMany(RatingDetail::class);
    }
}
