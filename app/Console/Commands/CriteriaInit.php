<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CriteriaInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'criteria:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Init criteria or reset";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $criteria = config('criteria');
        $criteriaValue = config('criteriavalue');
    }
}
