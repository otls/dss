<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SelectionValuesResource extends JsonResource
{
    public static $wrap = null;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = [];
        foreach ($this->resource as $key => $alternative) {
            $resource[$key][] = $alternative->alt_name;
            foreach ($alternative->ratingvalues as $k => $value) {
                $resource[$key][] = "{$value->label} ({$value->value})";
            }
        }
        return $resource;
    }
}
