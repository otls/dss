<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlternativeRequest;
use App\Models\Alternative;
use App\Models\Criteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class AlternativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->datatable($request);
        }

        return view('alternative.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('alternatives.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlternativeRequest $request)
    {
        try {
            $data = DB::transaction(function () use ($request) {
                $alternative = Alternative::create($request->only([
                    'alt_name', 'alt_description'
                ]));
                return $alternative;
            });

            $response = [
                'code' => 201,
                'data' => $data,
                'message' => 'Data successfuly added!'
            ];
        } catch (\Throwable $th) {
            $response = [
                'code' => 500,
                'message' => $th->getMessage()
            ];
        }
        return response()->json($response, $response['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Alternative $alternative)
    {
        return response()->json($alternative);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlternativeRequest $request, Alternative $alternative)
    {
        try {
            $alternative->alt_name = $request->alt_name;
            $alternative->alt_description = $request->alt_description;
            $data = $alternative->save();
            return response()->json($data);
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Alternative $alternative)
    {
        try {
            $alternative->delete();
            return response()->json([], 204);
        } catch (\Throwable $th) {
            return response()->json(500, $th->getMessage());
        }
    }

    public function datatable(Request $request)
    {
        return DataTables::eloquent((new Alternative())->newQuery())
            ->addIndexColumn()
            ->skipTotalRecords()
            ->toJson();
    }
}
