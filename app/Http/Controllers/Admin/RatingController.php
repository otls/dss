<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\HistoryStoreRequest;
use App\Http\Requests\RatingDetailStore;
use App\Http\Resources\SelectionValuesResource;
use App\Models\Alternative;
use App\Models\Criteria;
use App\Models\CriteriaValue;
use App\Models\History;
use App\Models\Rating;
use App\Models\RatingDetail;
use Illuminate\Http\Request;
use App\Modules\Moora;
use Illuminate\Support\Facades\DB;

class RatingController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $alternatives = Alternative::select('id', 'alt_name')
                ->get();
            return new SelectionValuesResource($alternatives);
        }

        $tableHeader = Criteria::getHeaderAttribute();

        $data = [
            'tableHeader' => $tableHeader
        ];

        return view('rating.index', $data);
    }

    public function show(Request $request, Alternative $alternative)
    {
        $alternative->append('rating_values');
        return view('rating.detail', compact('alternative'));
    }

    public function form(Request $request, Alternative $alternative)
    {
        $alternative->rating_values;
        $ratings = $alternative->rating_values;
        $criteria = Criteria::get()->append('value');

        $form = view('rating.form', [
            'criteria' => $criteria,
            'alternative' => $alternative,
            'ratings' => $ratings
        ])->render();

        return response()->json($form);
    }

    public function update(RatingDetailStore $request, Rating $rating)
    {
        $inputs = $request->validated();
        $alternative = $rating->alternative;

        foreach ($inputs as $crt_id => $critval_id) {
            $crt_id = str_replace('c_', "", $crt_id);
            $critval = CriteriaValue::findOrFail($critval_id);
            RatingDetail::updateOrCreate(
                [
                    'rating_id' => $rating->id,
                    'criteria_id' => $crt_id,
                ],
                [
                    'criteria_value_id' => $critval->id,
                    'rating_detail_label' => $critval->critval_label,
                    'rating_detail_value' => $critval->critval_value,
                ]
            );
        }
        return response()->json($alternative->append('rating_values'));
    }

    public function startCounting(Request $request)
    {
        abort_if(!$request->ajax(), 404);

        $criteria = Criteria::orderBy('id', 'asc')->get();
        $alternatives = Alternative::orderBy('alt_name', 'asc')->get();
        $header = Criteria::getHeaderWithHtml();

        $ratings = $alternatives->transform(function ($item) {
            $alt_data['alternative'] = $item->alt_name;
            foreach ($item->ratingvalues as $key => $value) {
                $alt_data['ratings'][] = $value->value;
            }
            return $alt_data;
        });

        $selection = new Moora($ratings->toArray(), $criteria->toArray());
        $selection = $selection->getAll();

        $result = view('moora.index', compact(
            'header',
            'ratings',
            'selection'
        ))->render();

        return response()->json($result);
    }

    public function saveHistory(HistoryStoreRequest $request)
    {
        $historyDetails = $request->results;
        // return response()->json($historyDetails);
        try {
            DB::transaction(function () use ($historyDetails) {
                $history = History::create();
                $historyDetails = collect($historyDetails)->sortByDesc('value');
                $rank = 1;

                foreach ($historyDetails as $k => $item) {
                    $item = [
                        'hd_value' => $item['value'],
                        'hd_alternative' => $item['alternative'],
                        'hd_rank' => "#" . $rank
                    ];
                    $rank++;
                    $history->details()->create($item);
                }
            });
            return response()->json(null, 201);
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
