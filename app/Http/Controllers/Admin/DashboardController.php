<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Alternative;
use App\Models\Criteria;
use App\Models\History;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $chart = History::latestFormatChart();

        $data = [
            'alternative' => Alternative::count(),
            'criteria' => Criteria::count(),
            'history' => History::count(),
            'chart' => $chart
        ];

        return view('admin.dashboard', $data);
    }
}
