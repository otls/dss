<?php

namespace App\Http\Requests;

use App\Models\Criteria;
use Illuminate\Foundation\Http\FormRequest;

class RatingDetailStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $criterias = Criteria::get();

        foreach ($criterias as $key => $criteria) {
            $rules["c_{$criteria->id}"] = ['required', 'integer','exists:criteria_values,id'];
        }
        return $rules;
    }
}
