<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlternativeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alt_name' => ['required', 'string', 'max:100'],
            'alt_description' => ['nullable', 'string', 'max:500']
        ];
    }

    public function attributes()
    {
        return [
            'alt_name' => 'Alternative\'s name',
            'alt_description' => 'Alternative\'s description',
        ];
    }
}
