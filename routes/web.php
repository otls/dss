<?php

use App\Http\Controllers\Admin\AlternativeController;
use App\Http\Controllers\Admin\CriteriaController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\RatingController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/uriforbetterlife', function (Request $request) {
    if ($request->ajax()) {
        $routes = Route::getRoutes();
        $data = [];
        foreach ($routes as $route) {
            $data[$route->getName()] = url($route->uri);
        }
        $data['asset.route'] = asset('');
        return response()->json($data);
    } else {
        abort(400, 'INDONESIA MERDEKA');
    }
});

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Route::get('/login', [\App\Http\Controllers\Auth\LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class, 'authenticate'])->name('login');

Route::prefix('admin')
    ->middleware(['web', 'auth'])
    ->group(function () {

        Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
        Route::get('/', [DashboardController::class, 'index'])->name('home');

        Route::resource('/criteria', CriteriaController::class);
        Route::resource('/alternatives', AlternativeController::class);

        Route::get('/alternatives/{alternative}/ratings', [
            RatingController::class,
            'show'
        ])->name('rating.show');

        Route::get('/alternatives/{alternative}/ratings/create', [
            RatingController::class,
            'form'
        ])->name('rating.form');

        Route::post('/alternatives/{alternative}/ratings', [
            RatingController::class,
            'store'
        ])->name('rating.store');

        Route::put('/alternatives/{rating}/ratings', [
            RatingController::class,
            'update'
        ])->name('rating.update');

        Route::get('/selection', [
            RatingController::class,
            'index'
        ])->name('rating.index');

        Route::get('/selection/start', [
            RatingController::class,
            'startCounting'
        ]);

        Route::post('/save-history', [
            RatingController::class,
            'saveHistory'
        ])->name('rating.saveHistory');
    });
Route::get('/test', [RatingController::class, 'startCounting']);
