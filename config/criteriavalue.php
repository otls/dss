<?php
return [
    [
        'critval_label' => 'Iya',
        'critval_tag' => 'karantina',
        'critval_value' => 5
    ],
    [
        'critval_label' => 'Tidak',
        'critval_tag' => 'karantina',
        'critval_value' => 1
    ],
    [
        'critval_label' => '< 1.500',
        'critval_tag' => 'harga',
        'critval_value' => 1
    ],
    [
        'critval_label' => '1.501 - 1.600',
        'critval_tag' => 'harga',
        'critval_value' => 2
    ],
    [
        'critval_label' => '1.601 - 1.700',
        'critval_tag' => 'harga',
        'critval_value' => 3
    ],
    [
        'critval_label' => '1.701 - 1.800',
        'critval_tag' => 'harga',
        'critval_value' => 4
    ],
    [
        'critval_label' => '> 1.800',
        'critval_tag' => 'harga',
        'critval_value' => 5
    ],
    [
        'critval_label' => '0-9,9 km',
        'critval_tag' => 'jarak',
        'critval_value' => 1
    ],
    [
        'critval_label' => '10-19,9 km',
        'critval_tag' => 'jarak',
        'critval_value' => 2
    ],
    [
        'critval_label' => '20- 29,9 km',
        'critval_tag' => 'jarak',
        'critval_value' => 3
    ],
    [
        'critval_label' => '< 30 km',
        'critval_tag' => 'jarak',
        'critval_value' => 4
    ],
    [
        'critval_label' => '< 30 km',
        'critval_tag' => 'jarak',
        'critval_value' => 4
    ],
    [
        'id' => "404",
        'critval_label' => 'uninitialized',
        'critval_tag' => 'uninitialized',
        'critval_value' => 0
    ],
    [
        'critval_label' => 'Fraksi 3',
        'critval_tag' => 'fraksi',
        'critval_value' => 7
    ],
    [
        'critval_label' => 'Fraksi 2',
        'critval_tag' => 'fraksi',
        'critval_value' => 6
    ],
    [
        'critval_label' => 'Fraksi 1',
        'critval_tag' => 'fraksi',
        'critval_value' => 5
    ],
    [
        'critval_label' => 'Fraksi 4',
        'critval_tag' => 'fraksi',
        'critval_value' => 4
    ],
    [
        'critval_label' => 'Fraksi 5',
        'critval_tag' => 'fraksi',
        'critval_value' => 3
    ],
    [
        'critval_label' => 'Fraksi 0',
        'critval_tag' => 'fraksi',
        'critval_value' => 2
    ],
    [
        'critval_label' => 'Fraksi 00',
        'critval_tag' => 'fraksi',
        'critval_value' => 1
    ],
    [
        'critval_label' => 'Bulat/lonjong tanpa cekungan',
        'critval_tag' => 'shape',
        'critval_value' => 5
    ],
    [
        'critval_label' => 'Terdapat cekungan-cekungan',
        'critval_tag' => 'shape',
        'critval_value' => 1
    ],
    [
        'critval_label' => 'Merah',
        'critval_tag' => 'color',
        'critval_value' => 1
    ],
    [
        'critval_label' => 'Merah gelap',
        'critval_tag' => 'color',
        'critval_value' => 2
    ],
    [
        'critval_label' => 'Kehitaman',
        'critval_tag' => 'color',
        'critval_value' => 3
    ],
    [
        'critval_label' => 'Hitam gelap',
        'critval_tag' => 'color',
        'critval_value' => 4
    ],
];
