<?php
return [
    [
        'crt_name' => 'Fraksi Kematangan',
        'crt_value_tag' => 'fraksi',
        'crt_weight' => 35,
        'crt_type' => 'benefit',
        'crt_description' => 'Tingkat fraksi kematangan kelapa sawit'
    ],
    [
        'crt_name' => 'Pernyataan Karantina',
        'crt_value_tag' => 'karantina',
        'crt_weight' => 25,
        'crt_type' => 'benefit',
        'crt_description' => 'Buah telah dikarantina dan dinyatakan sehat'
    ],
    [
        'crt_name' => 'Harga',
        'crt_value_tag' => 'harga',
        'crt_weight' => 25,
        'crt_type' => 'cost',
        'crt_description' => 'criteria description example'
    ],
    [
        'crt_name' => 'Jarak Pengambilan',
        'crt_value_tag' => 'jarak',
        'crt_weight' => 15,
        'crt_type' => 'cost',
        'crt_description' => 'Jarak pengambilan kelapa sawit dari pabrik'
    ],
    [
        'crt_name' => 'Warna Buah',
        'crt_value_tag' => 'color',
        'crt_weight' => 30,
        'crt_type' => 'benefit',
        'crt_description' => 'Warna kulit buah kelapa sawit'
    ],
    [
        'crt_name' => 'Bentuk Buah',
        'crt_value_tag' => 'shape',
        'crt_weight' => 30,
        'crt_type' => 'benefit',
        'crt_description' => 'Bentuk dari buah kelapa sawit'
    ],
];
