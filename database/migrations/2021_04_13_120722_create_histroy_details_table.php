<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistroyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('history_id', false, true);
            $table->string('hd_alternative', 255);
            $table->float('hd_value');
            $table->string('hd_rank', 10)->nullable();
            $table->timestamps();

            $table->foreign('history_id', 'history_details_history_id_foreign')
                ->references('id')
                ->on('histories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_details', function (Blueprint $table) {
            $table->dropForeign('history_details_history_id_foreign');
        });
        Schema::dropIfExists('history_details');
    }
}
