<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rating_id', false, true);
            $table->bigInteger('criteria_id', false, true);
            $table->bigInteger('criteria_value_id', false, true)->nullable();
            $table->string('rating_detail_label', 255);
            $table->float('rating_detail_value')->default(0);
            $table->timestamps();

            $table->foreign('rating_id', 'rating_details_rating_id_foreign')
                ->references('id')
                ->on('ratings')
                ->onDelete('cascade');

            $table->foreign('criteria_id', 'rating_details_criteria_id_foreign')
            ->references('id')
            ->on('criterias')
            ->onDelete('cascade');

            $table->foreign('criteria_value_id', 'rating_details_criteria_value_id_foreign')
            ->references('id')
            ->on('criteria_values')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rating_details', function (Blueprint $table) {
            $table->dropForeign('rating_details_rating_id_foreign');
            $table->dropForeign('rating_details_criteria_id_foreign');
            $table->dropForeign('rating_details_criteria_value_id_foreign');
        });
        Schema::dropIfExists('rating_details');
    }
}
