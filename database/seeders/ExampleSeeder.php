<?php

namespace Database\Seeders;

use App\Models\Alternative;
use App\Models\Criteria;
use App\Models\CriteriaValue;
use App\Models\Rating;
use App\Models\RatingDetail;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ExampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Factory::create();

        $alternatives = [
            [
                'id' => 111,
                'alt_name' => "A1 " . $faker->name,
                'alt_description' => "data test 1",
            ],
            [
                'id' => 222,
                'alt_name' => "A2 " . $faker->name,
                'alt_description' => "data test 2"
            ],
            [
                'id' => 333,
                'alt_name' => "A3 " . $faker->name,
                'alt_description' => "data test 3"
            ],
            [
                'id' => 444,
                'alt_name' => "A4 " . $faker->name,
                'alt_description' => "data test 4"
            ],
            [
                'id' => 555,
                'alt_name' => "A5 " . $faker->name,
                'alt_description' => "data test 5"
            ],
            [
                'id' => 666,
                'alt_name' => "A6 " . $faker->name,
                'alt_description' => "data test 6"
            ],
            [
                'id' => 777,
                'alt_name' => "A7 " . $faker->name,
                'alt_description' => "data test 7"
            ],
            [
                'id' => 888,
                'alt_name' => "A8 " . $faker->name,
                'alt_description' => "data test 8"
            ],
        ];

        $criterias = [
            [
                'id' => 111,
                'crt_name' => 'Material',
                'crt_weight' => 2.2, 'crt_value_tag' => 'numeric',
                'crt_type' => 'benefit'
            ],
            [
                'id' => 222,
                'crt_name' => 'Temperature Control',
                'crt_weight' => 2.1,
                'crt_value_tag' => 'numeric',
                'crt_type' => 'benefit'
            ],
            [
                'id' => 333,
                'crt_name' => 'Guaranty',
                'crt_weight' => 2.1,
                'crt_value_tag' => 'numeric',
                'crt_type' => 'benefit'
            ],
            [
                'id' => 444,
                'crt_name' => 'Price',
                'crt_weight' => 1.8,
                'crt_value_tag' => 'numeric',
                'crt_type' => 'cost'
            ],
            [
                'id' => 555,
                'crt_name' => 'Size',
                'crt_weight' => 1.8,
                'crt_value_tag' => 'numeric',
                'crt_type' => 'cost'
            ],
        ];

        $criteria_values = [];

        for ($i = 1; $i <= 100; $i++) {
            $criteria_values[] = [
                'id' => $i,
                'critval_label' => "Label {$i}",
                'critval_tag' => 'numeric',
                'critval_value' => $i,
            ];
        }

        $ratings = [];
        $i=1;
        foreach ($alternatives as $key => $value) {
            $ratings[$key] = [
                'id' => $i . $i . $i,
                'alternative_id' => $value['id'],
                'rating_periode' => date('Y-m-d H:i:s')
            ];
            $i++;
        }

        $rating_details = [
            ["criteria_id" => 111, "rating_id" => 111, 'rating_detail_label' => "label 111", "rating_detail_value" => 30, 'criteria_value_id' => 30],
            ["criteria_id" => 222, "rating_id" => 111, 'rating_detail_label' => "label 111", "rating_detail_value" => 20, 'criteria_value_id' => 20],
            ["criteria_id" => 333, "rating_id" => 111, 'rating_detail_label' => "label 111", "rating_detail_value" => 30, 'criteria_value_id' => 30],
            ["criteria_id" => 444, "rating_id" => 111, 'rating_detail_label' => "label 111", "rating_detail_value" => 50, 'criteria_value_id' => 50],
            ["criteria_id" => 555, "rating_id" => 111, 'rating_detail_label' => "label 111", "rating_detail_value" => 50, 'criteria_value_id' => 50],

            ["criteria_id" => 111, "rating_id" => 222, 'rating_detail_label' => "label 222", "rating_detail_value" => 40, 'criteria_value_id' => 40],
            ["criteria_id" => 222, "rating_id" => 222, 'rating_detail_label' => "label 222", "rating_detail_value" => 50, 'criteria_value_id' => 50],
            ["criteria_id" => 333, "rating_id" => 222, 'rating_detail_label' => "label 222", "rating_detail_value" => 30, 'criteria_value_id' => 30],
            ["criteria_id" => 444, "rating_id" => 222, 'rating_detail_label' => "label 222", "rating_detail_value" => 40, 'criteria_value_id' => 40],
            ["criteria_id" => 555, "rating_id" => 222, 'rating_detail_label' => "label 222", "rating_detail_value" => 30, 'criteria_value_id' => 30],

            ["criteria_id" => 111, "rating_id" => 333, "rating_detail_label" => "Label 333", "rating_detail_value" => 20, "criteria_value_id" => 20],
            ["criteria_id" => 222, "rating_id" => 333, "rating_detail_label" => "Label 333", "rating_detail_value" => 20, "criteria_value_id" => 20],
            ["criteria_id" => 333, "rating_id" => 333, "rating_detail_label" => "Label 333", "rating_detail_value" => 30, "criteria_value_id" => 30],
            ["criteria_id" => 444, "rating_id" => 333, "rating_detail_label" => "Label 333", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 555, "rating_id" => 333, "rating_detail_label" => "Label 333", "rating_detail_value" => 40, "criteria_value_id" => 40],

            ["criteria_id" => 111, "rating_id" => 444, "rating_detail_label" => "Label 444", "rating_detail_value" => 40, "criteria_value_id" => 40],
            ["criteria_id" => 222, "rating_id" => 444, "rating_detail_label" => "Label 444", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 333, "rating_id" => 444, "rating_detail_label" => "Label 444", "rating_detail_value" => 30, "criteria_value_id" => 30],
            ["criteria_id" => 444, "rating_id" => 444, "rating_detail_label" => "Label 444", "rating_detail_value" => 40, "criteria_value_id" => 40],
            ["criteria_id" => 555, "rating_id" => 444, "rating_detail_label" => "Label 444", "rating_detail_value" => 50, "criteria_value_id" => 50],



            ["criteria_id" => 111, "rating_id" => 555, "rating_detail_label" => "Label 555", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 222, "rating_id" => 555, "rating_detail_label" => "Label 555", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 333, "rating_id" => 555, "rating_detail_label" => "Label 555", "rating_detail_value" => 30, "criteria_value_id" => 30],
            ["criteria_id" => 444, "rating_id" => 555, "rating_detail_label" => "Label 555", "rating_detail_value" => 40, "criteria_value_id" => 40],
            ["criteria_id" => 555, "rating_id" => 555, "rating_detail_label" => "Label 555", "rating_detail_value" => 30, "criteria_value_id" => 30],

            ["criteria_id" => 111, "rating_id" => 666, "rating_detail_label" => "Label 666", "rating_detail_value" => 30, "criteria_value_id" => 30],
            ["criteria_id" => 222, "rating_id" => 666, "rating_detail_label" => "Label 666", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 333, "rating_id" => 666, "rating_detail_label" => "Label 666", "rating_detail_value" => 40, "criteria_value_id" => 40],
            ["criteria_id" => 444, "rating_id" => 666, "rating_detail_label" => "Label 666", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 555, "rating_id" => 666, "rating_detail_label" => "Label 666", "rating_detail_value" => 30, "criteria_value_id" => 30],



            ["criteria_id" => 111, "rating_id" => 777, "rating_detail_label" => "Label 77", "rating_detail_value" => 40, "criteria_value_id" => 40],
            ["criteria_id" => 222, "rating_id" => 777, "rating_detail_label" => "Label 77", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 333, "rating_id" => 777, "rating_detail_label" => "Label 77", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 444, "rating_id" => 777, "rating_detail_label" => "Label 77", "rating_detail_value" => 40, "criteria_value_id" => 40],
            ["criteria_id" => 555, "rating_id" => 777, "rating_detail_label" => "Label 77", "rating_detail_value" => 30, "criteria_value_id" => 30],

            ["criteria_id" => 111, "rating_id" => 888, "rating_detail_label" => "Label 88", "rating_detail_value" => 30, "criteria_value_id" => 30],
            ["criteria_id" => 222, "rating_id" => 888, "rating_detail_label" => "Label 88", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 333, "rating_id" => 888, "rating_detail_label" => "Label 88", "rating_detail_value" => 40, "criteria_value_id" => 40],
            ["criteria_id" => 444, "rating_id" => 888, "rating_detail_label" => "Label 88", "rating_detail_value" => 50, "criteria_value_id" => 50],
            ["criteria_id" => 555, "rating_id" => 888, "rating_detail_label" => "Label 88", "rating_detail_value" => 40, "criteria_value_id" => 40],

        ];
        Alternative::unguard();
        foreach ($alternatives as $key => $value) {
            Alternative::updateOrCreate(
                ['id' => $value['id']],
                $value
            );
        }

        Criteria::unguard();
        foreach ($criterias as $key => $value) {
            Criteria::updateOrCreate(
                ['id' => $value['id']],
                $value
            );
        }

        CriteriaValue::unguard();
        foreach ($criteria_values as $key => $value) {
            CriteriaValue::updateOrCreate(
                ['id' => $value['id']],
                $value
            );
        }

        Rating::unguard();
        foreach ($ratings as $key => $value) {
            Rating::updateOrCreate(
                ['id' => $value['id']],
                $value
            );
        }

        RatingDetail::unguard();
        foreach ($rating_details as $key => $value) {
            $value['id'] = $key.$key.$key;
            RatingDetail::updateOrCreate(
                ['id' => $value['id']],
                $value
            );
        }
    }
}
