<?php

namespace Database\Seeders;

use App\Models\Criteria;
use Illuminate\Database\Seeder;

class CriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $criterias = config('criteria');
        $this->command->info("Seeding Criteria");
        foreach ($criterias as $key => $criteria) {
            $criteria = Criteria::create($criteria);
            $this->command->info("{$criteria->crt_name}");
        }
        $this->command->info(PHP_EOL);
    }
}
