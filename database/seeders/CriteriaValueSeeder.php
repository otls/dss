<?php

namespace Database\Seeders;

use App\Models\CriteriaValue;
use Illuminate\Database\Seeder;

class CriteriaValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $criteriaValue = config('criteriavalue');
        $this->command->info("Seeding criteria value");
        foreach ($criteriaValue as $key => $critval) {
            $critval = CriteriaValue::create($critval);
            $this->command->info("{$critval->critval_label} {$critval->critval_value} {$critval->critval_tag}");
        }
        $this->command->info(PHP_EOL);
    }
}
