<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            'name' => 'Administrator',
            'email' => 'admin@mail.com',
            'password' => Hash::make('admin123')
        ];

        try {
            User::updateOrCreate(
                ['email' => $admin['email']],
                $admin
            );
        } catch (\Exception $th) {
            $this->command->error($th->getMessage());
        }
    }
}
